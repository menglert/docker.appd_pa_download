FROM ubuntu AS builder
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ARG BASEURL="https://download.appdynamics.com/download/prox/download-file"
ARG VERSION
ARG USER
ARG PASSWORD

ENV APPD_PLATFORM /opt/appdynamics/platform

ADD pa.response /tmp/
ADD install_pa.sh /tmp/

RUN chmod +x /tmp/*.sh
RUN /tmp/install_pa.sh


FROM ubuntu
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ENV APPD_PLATFORM /opt/appdynamics/platform

COPY --from=builder ${APPD_PLATFORM} ${APPD_PLATFORM}

ADD start.sh ${APPD_PLATFORM}/

RUN apt-get update \
    && apt-get install --fix-missing -q -y libaio1 libnuma1 lsof curl seccomp \
    && echo "ulimit -n 65535" >> /etc/profile \
    && echo "ulimit -u 8192" >> /etc/profile \
    && echo "vm.swappiness = 10" >> /etc/sysctl.conf

EXPOSE 9191

CMD [ "/bin/bash", "-c", "${APPD_PLATFORM}/start.sh" ]