#!/bin/bash

find $APPD_PLATFORM/ -name *.MYI -exec /bin/bash -c "echo {} | xargs $APPD_PLATFORM/mysql/bin/myisamchk" \;
find $APPD_PLATFORM/ -name *.MYI -exec /bin/bash -c "echo {} | sed \"s/.MYI//\" | xargs $APPD_PLATFORM/mysql/bin/myisamchk" \;

find $APPD_PLATFORM/ -name *.log -exec /bin/bash -c "rm -rf {}" \;
find $APPD_PLATFORM/ -name *.lock -exec /bin/bash -c "rm -rf {}" \;
find $APPD_PLATFORM/ -name *.lck -exec /bin/bash -c "rm -rf {}" \;
find $APPD_PLATFORM/ -name *.pid -exec /bin/bash -c "rm -rf {}" \;
find $APPD_PLATFORM/ -name *.id -exec /bin/bash -c "rm -rf {}" \;

$APPD_PLATFORM/platform-admin/bin/platform-admin.sh start-platform-admin
sleep 30
$APPD_PLATFORM/platform-admin/bin/platform-admin.sh login -u admin -p appdynamics

tail -f /dev/null